FROM docker:19.03.1
# Haroldo Rios <haroldodelrio@gmail.com>

RUN apk update && apk add --no-cache\
	ca-certificates \
	groff \
	less \
	python \
	py-pip \
	&& rm -rf /var/cache/apk/* \
  && pip install pip --upgrade \
  && pip install awscli